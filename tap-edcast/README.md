# tap-edcast

`tap-edcast` is a Singer tap for edcast.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.

## Installation

```bash
pipx install git+https://gitlab.com/gitlab-data/meltano_taps.git#subdirectory=tap-edcast
```

## Configuration

### Accepted Config Options

```bash
REQUIRED: tap_edcast_username   - username for the API
REQUIRED: tap_edcast_password   - password for the API
OPTIONAL: tap_edcast_start_date - start_date to fetch data set
OPTIONAL: tap_edcast_end_date   - end_date to fetch data set
``` 

A full list of supported settings and capabilities for this
tap is available by running:

```bash
poetry run tap-edcast --about [--format json]
```

### Source Authentication and Authorization

Authentication works via `config.json` (added to `/.secrets` for coverage in `gitignore`)

Example
```json
{
    "start_date": "2021-12-01T03:17:54Z",
    "end_date": "2021-12-06T03:17:56Z",
    "username": "<key_username>",
    "password": "<key_password>",
    "url_base": "https://api.domo.com"
}
```

## Usage

You can easily run `tap-edcast` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly
```bash
poetry run tap-edcast --config .secrets/config.json
```

Also, you can run some utils to check `tap-edcast` in details
```bash
tap-edcast --version
tap-edcast --help
```

## Developer Resources

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

Your project comes with a custom `meltano.yml` project file already created. Open the `meltano.yml` and follow any _"TODO"_ items listed in
the file.

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-edcast
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-edcast --version
# OR run a test `elt` pipeline:
# !!! NOTE !!!
# As there is known bug for target-jsonl, 
# create a directory before you run a script 
# allows data land into default /output folder without error
# mkdir output 
meltano elt tap-edcast target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to 
develop your own taps and targets.
