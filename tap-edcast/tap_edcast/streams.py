"""Stream type classes for tap-edcast."""
import logging
import json
from typing import Optional, Iterable, cast, Any, Dict
from pathlib import Path

import requests


from tap_edcast.client import EdcastStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
VERSION = "/v1"


class DatasetsStream(EdcastStream):
    """Define stream: datasets
    Docs: https://docs.edcast.com/docs/datasets-definition
    This stream retrieves list of datasets available for EdCast.
    No custom implementation, as it is typucal GET request with basic parameters
    """

    primary_keys = ["id"]

    replication_key = None
    replicaton_method = "FULL"
    records_jsonpath: str = "$[*]"

    name = "datasets"
    path = f"{VERSION}/datasets"
    schema_filepath = SCHEMAS_DIR / "datasets.json"


class GlueGroupsG3GroupPerformanceDataExplorerStreams(EdcastStream):
    """Define stream: 300121_Glue_ Groups [G][3] [Group Performance Data Explorer]

    Docs:
    https://docs.edcast.com/docs/datasets-definition#groups-g-group-performance-data-explorer
    """

    primary_keys = [
        "ECL ID",
        "Time",
        "Group Name",
        "Card Resource URL",
        "Assigned Content",
        "Event",
        "Card Author Full Name",
        "Follower (User Full Name)",
        "Following (User Full Name)",
    ]
    replication_key = "Time"
    replicaton_method = "FULL"

    name = "glue_groups_g3_group_performance_data_explorer"
    path = f"{VERSION}/datasets/query/execute/c1843a40-dac2-426d-bba4-1e35cd24dfe5"

    rest_method = "POST"

    schema_filepath = (
        SCHEMAS_DIR / "glue_groups_g3_group_performance_data_explorer.json"
    )

    @property
    def http_headers(self) -> dict:
        """
        Override header to fill it with Bearer token

        Returns:
            Dictionary of HTTP headers to use as a base for every request.
        """
        headers = {}
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = f"Bearer  {self.get_token}"

        return headers

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[Dict[Any, Any]]:
        """
        For the dataset, need to have payload with the SQL statement want to execute
        in order to retrieve the data.

        Args:
            context: dict - inherited parameter, not important at the moment
            next_page_token - inherited parameter, not important at the moment

        Returns:
            payload: str - as per definition it is dict,
            but Stream class like to digest `str` data type
            -----------------------------------------------
            i.e.:
            {"sql":
            "SELECT *
            FROM table
            WHERE time >= '2021-12-01T03:17:54'
            AND time < '2021-12-06T03:17:56'"}
        """
        start_date: str = self.config["start_date"]
        end_date: str = self.config["end_date"]

        payload: dict = {
            "sql": f"SELECT * "
            f"FROM table "
            f"WHERE time >= '{start_date}' "
            f"AND time < '{end_date}'"
        }

        return payload

    def _request(
        self, prepared_request: requests.PreparedRequest, context: Optional[dict]
    ) -> requests.Response:
        """
        Overwritten function to make request call suitable for this use case:
        - POST method with payload parameters

        Args:
            prepared_request: in case request is prepared in advance,
            should be passed, in this case it is not needed

            context: Stream partition or context dictionary.

        Returns:
            response: requests.Response
        """

        headers: dict = self.http_headers
        payload = json.dumps(
            self.prepare_request_payload(context=context, next_page_token=None)
        )

        response = requests.request(
            method=self.rest_method,
            url=f"{self.url_base}{self.path}",
            headers=headers,
            data=payload,
        )

        if self._LOG_REQUEST_METRICS:
            extra_tags = {}
            if self._LOG_REQUEST_METRIC_URLS:
                extra_tags["url"] = cast(str, prepared_request.path_url)
            self._write_request_duration_log(
                endpoint=self.path,
                response=response,
                context=context,
                extra_tags=extra_tags,
            )
        self.validate_response(response)
        logging.debug("Response received successfully.")
        return response

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows.
        For this stream, the result is returned as a list, and
        need to zip() valued with column list
        """
        column_list = response.json().get("columns")
        rows = response.json().get("rows")

        for row in rows:
            yield dict(zip(column_list, row))


# define STREAM_TYPES
STREAM_TYPES = [
    DatasetsStream,
    GlueGroupsG3GroupPerformanceDataExplorerStreams,
]
