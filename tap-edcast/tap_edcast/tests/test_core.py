"""Tests standard tap features using the built-in SDK tests library."""
import json
from singer_sdk.testing import get_standard_tap_tests
from tap_edcast.tap import Tapedcast

with open(file="../../.secrets/config.json", mode="r", encoding="utf-8") as jsonfile:
    TEST_CONFIG = json.load(jsonfile)


# Run standard built-in tap tests from the SDK:
def test_standard_tap_tests():

    """Run standard tap tests from the SDK."""
    tests = get_standard_tap_tests(Tapedcast, config=TEST_CONFIG)
    for test in tests:
        test()
