"""EdCast tap class.
   Source: https://www.edcast.com/
   Documentation source: https://docs.edcast.com/docs/edgraph-export
"""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schemas typing helpers

from tap_edcast.streams import STREAM_TYPES


class Tapedcast(Tap):
    """EdCast tap class."""

    name = "tap-edcast"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "tap_edcast_start_date",
            th.DateTimeType,
            description="Date from retrieve the data",
        ),
        th.Property(
            "tap_edcast_end_date",
            th.DateTimeType,
            description="Date to retrieve the data",
        ),
        th.Property(
            "username",
            th.StringType,
            required=True,
            description="Username for EdCast RESTful API access",
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
            description="Password for EdCast RESTful API access",
        ),
        th.Property(
            "url_base",
            th.StringType,
            required=True,
            description="Basic endpoint URL",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


cli = Tapedcast.cli
