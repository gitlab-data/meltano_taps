"""REST client handling, including tap-edcastStream base class."""
import requests
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BearerTokenAuthenticator


class EdcastStream(RESTStream):
    """tap-edcast stream class."""

    @property
    def get_token(self) -> str:
        """
        Provide access_token to retrieve data via RESTful API
        """
        method = "POST"
        url = self.config["url_base"]
        path = "/oauth/token?grant_type=client_credentials&scope=data"
        username = self.config["username"]
        password = self.config["password"]

        response = requests.request(
            method=method, url=f"{url}{path}", auth=(username, password)
        )

        response.raise_for_status()

        access_token = response.json().get("access_token")

        return access_token

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["url_base"]

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(self, self.get_token)
