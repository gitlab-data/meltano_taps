import vcr
from tap_adaptive.streams import (
    STREAM_TYPES,
    AccountsStream,
    ActiveCurrenciesStream,
    AttributesStream,
    CustomerLogoStream,
    DimensionFamiliesStream,
    DimensionMappingStream,
    DimensionsStream,
    GroupsStream,
    InstancesStream,
    LevelsStream,
    LocalesStream,
    ModeledSheetStream,
    RolesStream,
    PermissionSetsStream,
    SecurityAuditStream,
    SheetsStream,
    SheetDefinitionStream,
    TimeStream,
    TransactionDefinitionStream,
    UsersStream,
    VersionsStream,
)

streams_vcr = vcr.VCR(
    cassette_library_dir="./tap_adaptive/tests/cassettes/test_streams"
)


@streams_vcr.use_cassette()
def test_accounts_stream(test_tap):
    stream = AccountsStream(tap=test_tap)
    assert stream.name == "accounts"
    assert stream.method == "exportAccounts"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.accounts.account[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_active_currencies_stream(test_tap):
    stream = ActiveCurrenciesStream(tap=test_tap)
    assert stream.name == "active_currencies"
    assert stream.method == "exportActiveCurrencies"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.currencies.currency[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_attributes_stream(test_tap):
    stream = AttributesStream(tap=test_tap)
    assert stream.name == "attributes"
    assert stream.method == "exportAttributes"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.attributes.attribute[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_customer_logo_stream(test_tap):
    stream = CustomerLogoStream(tap=test_tap)
    assert stream.name == "customer_logo"
    assert stream.method == "exportCustomerLogo"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.logos.logo[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_dimension_families_stream(test_tap):
    stream = DimensionFamiliesStream(tap=test_tap)
    assert stream.name == "dimension_families"
    assert stream.method == "exportDimensionFamilies"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.families.family[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_dimensions_stream(test_tap):
    stream = DimensionsStream(tap=test_tap)
    assert stream.name == "dimensions"
    assert stream.method == "exportDimensions"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.dimensions.dimension[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


def test_dimension_mapping_stream(test_tap):
    stream = DimensionMappingStream(tap=test_tap)
    assert stream.name == "dimension_mapping"
    assert stream.method == "exportDimensionMapping"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.mappings[*]"
    assert stream.parent_stream_type == DimensionsStream
    assert type(stream) in STREAM_TYPES

    payload = stream._additional_payload(
        {"dimension_name": "test_dimension_name", "version_name": "test_version_name"},
        None,
    )
    expected_payload = (
        '<version name="test_version_name"/>'
        + "<dimensions>"
        + '<dimension name="test_dimension_name"/>'
        + "</dimensions>"
    )
    assert payload == expected_payload


@streams_vcr.use_cassette()
def test_groups_stream(test_tap):
    stream = GroupsStream(tap=test_tap)
    assert stream.name == "groups"
    assert stream.method == "exportGroups"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.groups.group[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    # The test credentials used did not have any groups return
    # assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_instances_stream(test_tap):
    stream = InstancesStream(tap=test_tap)
    assert stream.name == "instances"
    assert stream.method == "exportInstances"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.instances.instance[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_levels_stream(test_tap):
    stream = LevelsStream(tap=test_tap)
    assert stream.name == "levels"
    assert stream.method == "exportLevels"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.levels.level[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_locales_stream(test_tap):
    stream = LocalesStream(tap=test_tap)
    assert stream.name == "locales"
    assert stream.method == "exportLocales"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.locales.locale[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_roles_stream(test_tap):
    stream = RolesStream(tap=test_tap)
    assert stream.name == "roles"
    assert stream.method == "exportRoles"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.roles.role[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_permission_sets_stream(test_tap):
    stream = PermissionSetsStream(tap=test_tap)
    assert stream.name == "permission_sets"
    assert stream.method == "exportPermissionSets"
    assert stream.primary_keys == ["_data"]
    assert (
        stream.records_jsonpath == "$.response.output.permission_sets.permission_set[*]"
    )
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_security_audit_stream(test_tap):
    stream = SecurityAuditStream(tap=test_tap)
    assert stream.name == "security_audit"
    assert stream.method == "exportSecurityAudit"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.audit.event[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_sheets_stream(test_tap):
    stream = SheetsStream(tap=test_tap)
    assert stream.name == "sheets"
    assert stream.method == "exportSheets"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.sheets"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record

    assert len(test_tap.inter_stream_data[stream.name]) > 0


@streams_vcr.use_cassette()
def test_sheet_definition_stream(test_tap):
    list(SheetsStream(tap=test_tap).get_records(None))
    stream = SheetDefinitionStream(tap=test_tap)
    assert stream.name == "sheet_definition"
    assert stream.method == "exportSheetDefinition"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.modeled-sheet"
    assert type(stream) in STREAM_TYPES

    sheets_index = STREAM_TYPES.index(SheetsStream)
    sheet_definition_index = STREAM_TYPES.index(SheetDefinitionStream)
    assert sheets_index < sheet_definition_index

    records = list(stream.get_records(None))
    assert len(records) == len(test_tap.inter_stream_data[SheetsStream.name])
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


def test_modeled_sheet_stream(test_tap):
    stream = ModeledSheetStream(tap=test_tap)
    assert stream.name == "modeled_sheet"
    assert stream.method == "exportModeledSheet"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.ConfigurableModel"
    assert stream.parent_stream_type == SheetDefinitionStream
    assert type(stream) in STREAM_TYPES

    payload = stream._additional_payload({"name": "test_name"}, None)
    assert payload == '<exportModel sheetToExport="test_name"/>'


@streams_vcr.use_cassette()
def test_time_stream(test_tap):
    stream = TimeStream(tap=test_tap)
    assert stream.name == "time"
    assert stream.method == "exportTime"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.time[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_transaction_definition_stream(test_tap):
    stream = TransactionDefinitionStream(tap=test_tap)
    assert stream.name == "transaction_definition"
    assert stream.method == "exportTransactionDefinition"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.transaction[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_users_stream(test_tap):
    stream = UsersStream(tap=test_tap)
    assert stream.name == "users"
    assert stream.method == "exportUsers"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.users.user[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record


@streams_vcr.use_cassette()
def test_versions_stream(test_tap):
    stream = VersionsStream(tap=test_tap)
    assert stream.name == "versions"
    assert stream.method == "exportVersions"
    assert stream.primary_keys == ["_data"]
    assert stream.records_jsonpath == "$.response.output.versions.version[*]"
    assert type(stream) in STREAM_TYPES

    records = list(stream.get_records(None))
    assert len(records) > 0
    for record in records:
        assert "_data" in record
        assert record["_data"] is not "null"
        assert "_time_extracted" in record
