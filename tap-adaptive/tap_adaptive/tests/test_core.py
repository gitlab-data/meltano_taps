"""Tests standard tap features using the built-in SDK tests library."""
import vcr

streams_vcr = vcr.VCR(cassette_library_dir="./tap_adaptive/tests/cassettes/test_core")

from singer_sdk.testing import get_standard_tap_tests

from tap_adaptive.tap import TapAdaptive


# Run standard built-in tap tests from the SDK:
# @streams_vcr.use_cassette()
def test_standard_tap_tests(test_config):
    """Run standard tap tests from the SDK."""
    tests = get_standard_tap_tests(
        TapAdaptive,
        config=test_config,
    )
    for test in tests:
        test()
