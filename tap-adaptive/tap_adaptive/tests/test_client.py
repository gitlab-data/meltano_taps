import pytest
import json
import requests
import responses
import xmltodict


@pytest.fixture
def xml_payload(test_stream, test_config):
    return (
        '<?xml version="1.0" encoding="UTF-8"?>'
        + f'<call method="{test_stream.method}" callerName="tap-adaptive">'
        + f"<credentials login=\"{test_config['username']}\" password=\"{test_config['password']}\"/>"
        + "</call>"
    )


@pytest.fixture
def xml_response():
    return (
        "<?xml version='1.0' encoding='UTF-8'?>"
        + '<response success="true">'
        + "<output>"
        + '<users seqNo="55">'
        + '<user id="1" name="Jane Doe">'
        + '<address street="123 Main St" city="Hometown"/>'
        + '<pets num_pets="2">'
        + '<pet name="Spot" type="Dog"/>'
        + '<pet name="Whiskers" type="Cat"/>'
        + "</pets>"
        + '<sibling name="Steve"/>'
        + "</user>"
        + '<user id="2" name="Joe Doe"/>'
        + "</users>"
        + "</output>"
        + "</response>"
    )


@pytest.fixture
def xml_to_dict(xml_response):
    return xmltodict.parse(xml_response)


def test_client_attributes(test_stream):
    assert test_stream.http_headers == {}
    assert test_stream.rest_method == "POST"


def test_prepare_request_payload(test_stream, xml_payload):
    assert test_stream.prepare_request_payload(None, None) == xml_payload


def test_get_url_params(test_stream):
    assert test_stream.get_url_params(None, None) == {}


def test_get_next_page_token(test_stream):
    assert not test_stream.get_next_page_token(None, None)


def test_prepare_request(test_stream, xml_payload):
    prepared_request = test_stream.prepare_request(None, None)
    assert prepared_request.method == "POST"
    assert prepared_request.url == test_stream.url_base
    assert prepared_request.body == xml_payload


@responses.activate
def test_parse_response(test_stream, xml_response, xml_payload):
    expected_records = xmltodict.parse(xml_response)["response"]["output"]["users"][
        "user"
    ]
    responses.add(responses.POST, test_stream.url_base, body=xml_response, status=200)
    resp = requests.post(test_stream.url_base, data=xml_payload)
    records = list(test_stream.parse_response(resp))
    assert len(records) > 0
    for row in records:
        assert row in expected_records


@responses.activate
def test_post_process(test_stream, xml_response, xml_to_dict):
    responses.add(responses.POST, test_stream.url_base, body=xml_response, status=200)
    resp = requests.post(test_stream.url_base, data=xml_payload)
    record = next(iter(test_stream.parse_response(resp)))
    processed_record = test_stream.post_process(record, None)
    assert "_data" in processed_record
    assert "_time_extracted" in processed_record
