import pytest
import os
from dotenv import load_dotenv

from tap_adaptive.tap import TapAdaptive
from tap_adaptive.client import AdaptiveStream
from singer_sdk import typing as th  # JSON Schema typing helpers

load_dotenv()


@pytest.fixture
def test_config():
    return {
        "username": os.getenv("TAP_ADAPTIVE_USERNAME"),
        "password": os.getenv("TAP_ADAPTIVE_PASSWORD"),
    }


@pytest.fixture
def test_tap(test_config):
    return TapAdaptive(config=test_config)


@pytest.fixture
def test_schema():
    return th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


@pytest.fixture
def test_stream(test_tap, test_schema):
    class TestStream(AdaptiveStream):
        name = "test_stream"
        method = "testStream"
        schema = test_schema
        primary_keys = ["id"]
        replication_key = None
        records_jsonpath = "$.response.output.users.user[*]"

    return TestStream(tap=test_tap)
