"""Stream type classes for tap-adaptive."""
import json
import requests
import xmltodict
from typing import Any, Dict, Optional, Iterable, cast

from singer_sdk import typing as th  # JSON Schema typing helpers
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_adaptive.client import AdaptiveStream


class AccountsStream(AdaptiveStream):
    name = "accounts"
    method = "exportAccounts"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.accounts.account[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class ActiveCurrenciesStream(AdaptiveStream):
    name = "active_currencies"
    method = "exportActiveCurrencies"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.currencies.currency[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class AttributesStream(AdaptiveStream):
    name = "attributes"
    method = "exportAttributes"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.attributes.attribute[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class CustomerLogoStream(AdaptiveStream):
    name = "customer_logo"
    method = "exportCustomerLogo"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.logos.logo[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class DimensionFamiliesStream(AdaptiveStream):
    name = "dimension_families"
    method = "exportDimensionFamilies"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.families.family[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


# class DimensionsStream(AdaptiveStream):
#     name = "dimensions"
#     method = "exportDimensions"
#     primary_keys = ["_data"]
#     records_jsonpath = "$.response.output.dimensions.dimension[*]"
#     schema = th.PropertiesList(
#         th.Property("_data", th.StringType),
#         th.Property("_time_extracted", th.DateTimeType),
#     ).to_dict()
#     parent_stream_type = VersionsStream


class GroupsStream(AdaptiveStream):
    name = "groups"
    method = "exportGroups"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.groups.group[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class InstancesStream(AdaptiveStream):
    name = "instances"
    method = "exportInstances"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.instances.instance[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class LevelsStream(AdaptiveStream):
    name = "levels"
    method = "exportLevels"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.levels.level[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class LocalesStream(AdaptiveStream):
    name = "locales"
    method = "exportLocales"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.locales.locale[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class RolesStream(AdaptiveStream):
    name = "roles"
    method = "exportRoles"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.roles.role[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class PermissionSetsStream(AdaptiveStream):
    name = "permission_sets"
    method = "exportPermissionSets"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.permission_sets.permission_set[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class SecurityAuditStream(AdaptiveStream):
    name = "security_audit"
    method = "exportSecurityAudit"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.audit.event[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class SheetDefinitionStream(AdaptiveStream):
    name = "sheet_definition"
    method = "exportSheetDefinition"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.modeled-sheet"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        name = json.loads(record["_data"])["@name"]
        return {"name": name}

    def get_next_page_token(
        self, response: requests.Response, previous_token: int
    ) -> Optional[Any]:
        if not previous_token:
            previous_token = 0
        try:
            next_index_token = previous_token + 1
            self._tap.inter_stream_data[SheetsStream.name][next_index_token]
            return next_index_token
        except IndexError:
            return None

    def _additional_payload(self, context: Optional[dict], next_page_token: int) -> str:
        if not next_page_token:
            next_page_token = 0
        sheet_id = self._tap.inter_stream_data[SheetsStream.name][next_page_token]
        return f'<modeled-sheet id="{sheet_id}"/>'


class ModeledSheetStream(AdaptiveStream):
    name = "modeled_sheet"
    method = "exportModeledSheet"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.ConfigurableModel"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()
    parent_stream_type = SheetDefinitionStream
    ignore_parent_replication_key = True

    def _additional_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> str:
        return f"<exportModel sheetToExport=\"{context['name']}\"/>"


class SheetsStream(AdaptiveStream):
    name = "sheets"
    method = "exportSheets"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.sheets"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        response_dict = xmltodict.parse(response.text)
        modeled_sheets = response_dict["response"]["output"]["sheets"]["modeled-sheet"]
        for modeled_sheet in modeled_sheets:
            self._tap.inter_stream_data[self.name].append(modeled_sheet["@id"])
        yield from extract_jsonpath(self.records_jsonpath, input=response_dict)


class TimeStream(AdaptiveStream):
    name = "time"
    method = "exportTime"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.time[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class TransactionDefinitionStream(AdaptiveStream):
    name = "transaction_definition"
    method = "exportTransactionDefinition"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.transaction[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class UsersStream(AdaptiveStream):
    name = "users"
    method = "exportUsers"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.users.user[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()


class VersionsStream(AdaptiveStream):
    name = "versions"
    method = "exportVersions"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.versions.version[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        name = json.loads(record["_data"])["@name"]
        return {"version_name": name}


class DimensionsStream(AdaptiveStream):
    name = "dimensions"
    method = "exportDimensions"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.dimensions.dimension[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()
    parent_stream_type = VersionsStream
    ignore_parent_replication_key = True

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        context["dimension_name"] = json.loads(record["_data"])["@name"]
        return context


class DimensionMappingStream(AdaptiveStream):
    name = "dimension_mapping"
    method = "exportDimensionMapping"
    primary_keys = ["_data"]
    records_jsonpath = "$.response.output.mappings[*]"
    schema = th.PropertiesList(
        th.Property("_data", th.StringType),
        th.Property("_time_extracted", th.DateTimeType),
    ).to_dict()
    parent_stream_type = DimensionsStream
    ignore_parent_replication_key = True

    def _additional_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> str:
        payload = (
            f"<version name=\"{context['version_name']}\"/>"
            + "<dimensions>"
            + f"<dimension name=\"{context['dimension_name']}\"/>"
            + "</dimensions>"
        )
        return payload


STREAM_TYPES = [
    AccountsStream,
    ActiveCurrenciesStream,
    AttributesStream,
    CustomerLogoStream,
    DimensionFamiliesStream,
    DimensionsStream,
    GroupsStream,
    InstancesStream,
    LevelsStream,
    LocalesStream,
    RolesStream,
    PermissionSetsStream,
    SecurityAuditStream,
    SheetsStream,
    SheetDefinitionStream,
    ModeledSheetStream,
    TimeStream,
    TransactionDefinitionStream,
    UsersStream,
    VersionsStream,
    DimensionMappingStream,
]
