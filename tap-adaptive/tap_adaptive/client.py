"""REST client handling, including adaptiveStream base class."""

import json
import requests
import xmltodict
from pathlib import Path
from typing import Any, Dict, Optional, Iterable, cast

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer.utils import now as time_now


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class AdaptiveStream(RESTStream):
    """adaptive stream class."""

    url_base = "https://api.adaptiveinsights.com/api/v29"
    rest_method = "POST"
    path = ""
    method = ""

    @property
    def http_headers(self) -> Dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        """This API returns all records in each call"""
        return None

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> str:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """

        """The API uses xml body to call endpoints"""
        return (
            '<?xml version="1.0" encoding="UTF-8"?>'
            + f'<call method="{self.method}" callerName="{self.tap_name}">'
            + f"<credentials login=\"{self.config['username']}\" password=\"{self.config['password']}\"/>"
            + self._additional_payload(context, next_page_token)
            + "</call>"
        )

    def _additional_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> str:
        """Return additional xml elements required for the endpoint"""
        """Return an empty string if no additional payload is needed"""
        return ""

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:
        """Prepare a request object.

        If partitioning is supported, the `context` object will contain the partition
        definitions. Pagination information can be parsed from `next_page_token` if
        `next_page_token` is not None.
        """
        http_method: str = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data: str = self.prepare_request_payload(context, next_page_token)
        headers: dict = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    data=request_data,
                )
            ),
        )
        return request

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        response_dict = xmltodict.parse(response.text)
        yield from extract_jsonpath(self.records_jsonpath, input=response_dict)

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        return {
            "_data": json.dumps(row),
            # "_time_extracted": datetime.datetime.now(),
            "_time_extracted": time_now(),
        }
