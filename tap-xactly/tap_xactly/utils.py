import os
import json

def get_abs_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)

def get_column_names(table):
    """ Get a comma separated list of columns specified in the schema file."""

    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, f'schemas/{table}.json')

    with open(filename, 'r') as jsonfile:
        data = json.load(jsonfile)

    columns = []
    for entry in data['properties']:
        columns.append(entry)

    return ", ".join(columns)
